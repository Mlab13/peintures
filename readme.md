# Peintures
Peintures est un site présentant des peintures.

lien gitlab: [GitLab "peintures"](https://gitlab.com/Mlab13/peintures)

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony):

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```
### Lancer des tests
```bash
php bin/phpunit --testdox
```
**commandes symfony**
```bash
symfony console make:entity
symfony console make:migration
symfony console d:m:m
symfony console make:unit-test
```
**Jour 1**
- Mise en place du Travail à effectuer avec Trello
- Création d'un UML avec Lucidchart
- Initialisation du projet
```bash
git init
git remote...
git add .
git commit -m "Iniatil commit"
git push
```
- Mise en place de GitLab pour le projet
- Création d'une pipeline pour le projet

**Jour 2**
- Création de la branche "feature/docker" pour travailler sur le projet et ensuite effectuer une merge sur gitlab lors de la fin du travail.
- Supprimer aussi la branche feature en local
```bash
git checkout master
git pull
git branch -d feature/docker
```
- Mise en place de l'environnement docker avec docker-compose
- Création des entitées
- Création des tests unitaires

**Jour 3**
- Mise en place du Webpack encore
```bash
composer require symfony/webpack-encore-bundle
npm install --force

npm run build `crée des fichiers js dans dossier public build`

npm install postcss-loader autoprefixer --dev
```
- Création du fichier postcss.config.js
- Mise en place de Bootstrap5
```bash
npm install bootstrap@next
npm install @popperjs/core

symfony console make:controller Home
```
- Création du fichier controller Home `dans Controller et templates`
- Visualisation de cette page `https://127.0.0.1:8001/home` 
- exemple de code de navbar de bootstrap5 + voir le rendu sur mobile avec inspecter
- Mise en place du thème du site
- Création du dossier js dans assets avec les fichiers.js
- Création du dossier scss avec les fichiers .scss et le dossier theme dans le dossier styles
- Création de la branche feature/template pour ce sprint
- 

*En suivant le tuto sur Symfony5 de Yoandev, je travaille sur la méthode agile en effectuant des sprints : une vidéo = un sprint, organisée pendant toute la durée du tuto. J'apprends en faisant et refaisant lorsque je rencontre un problème ou mauvaise manipulation.*